import express from 'express';
import { deleteUserById, putUserById, patchUserById, createUser, getUserById, getAllUsers } from '../route-handlers/users.route-handler.js'; 

const { DB_PATH } = process.env;

let usersRouter = express.Router();
usersRouter.use( express.json() );


usersRouter.get('/', async (req: express.Request, res: express.Response, next: express.NextFunction): Promise<void> => {
    try {
        const response = await getAllUsers();
        res.status(response.status).json(response);
    } catch (err) {
        next(err);
    }
})

usersRouter.get('/:id', async (req: express.Request, res: express.Response, next: express.NextFunction): Promise<void> => {
    try {
        const response = await getUserById(Number(req.params.id));
        res.status(response.status).json(response);
    } catch(err) {
        next(err);
    }
})

usersRouter.post('/', async (req: express.Request, res: express.Response, next: express.NextFunction): Promise<void> => {
    try {
        const response = await createUser(req.body);
        res.status(response.status).json(response);
    } catch (err) {
        next(err);
    }
})

usersRouter.patch('/:id', async (req: express.Request, res: express.Response, next: express.NextFunction): Promise<void> => {
    try {
        const response = await patchUserById(Number(req.params.id), req.body);
        res.status(response.status).json(response);
    } catch (err) {
        next(err);
    }              
})

usersRouter.put('/:id', async (req: express.Request, res: express.Response, next: express.NextFunction): Promise<void> => {
    try {
        const response = await putUserById(Number(req.params.id), req.body);
        res.status(response.status).json(response);
    } catch (err) {
        next(err);
    }           
})

usersRouter.delete('/:id', async (req: express.Request, res: express.Response, next: express.NextFunction): Promise<void> => {
    try {
        const response = await deleteUserById(Number(req.params.id));
        res.status(response.status).json(response);
    } catch (err) {
        next(err);
    }
})

export default usersRouter;