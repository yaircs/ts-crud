import express from 'express';
import log from '@ajar/marker';
import db from './db/db.userModel.js';
import usersRouter from './routes/users.routes.js';
import { reqLogger, errLogger, respondWithError } from './middlewares/middlewares.js';

const { PORT, HOST, DB_PATH, USERS_LOG_PATH, USERS_ERR_LOG_PATH } = process.env;

db.init();
const app = express();

app.use( express.json() );
app.use( reqLogger(USERS_LOG_PATH as string) );

app.use('/users', usersRouter);

app.use( errLogger(USERS_ERR_LOG_PATH as string) );
app.use( respondWithError );

app.use('*', (req: express.Request, res: express.Response): void => {
    res.status(200).send(`<h3>-404- not found</h3>`)
})

app.listen(Number(PORT), String(HOST), ()=> {
    log.magenta(`🌎  listening on`,`http://${HOST}:${PORT}`) 
})