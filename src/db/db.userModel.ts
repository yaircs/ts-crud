import { IDb } from '../types/types.js';
import fs from 'fs/promises';

const { DB_PATH } = process.env;

class Database {

    path = DB_PATH;

    async init() {
        const db = await this.read(DB_PATH as string);
        if(!db?.users) await this.save(DB_PATH as string, {users: []});
    }

    async read(path: string): Promise<IDb>{
        let data;
        try {            
            data = await fs.readFile(path, 'utf-8');
        } finally {
            return data ? JSON.parse(
                data.length ? data : `{}`
            ): undefined;
        }       
    }
    
    async save(path: string, data: object = {}): Promise<void> {
        await fs.writeFile(path, JSON.stringify(data, null, 2));
    }

}

const instance = new Database();
export default instance;