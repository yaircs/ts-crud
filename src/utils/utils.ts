import { IDb } from '../types/types.js';


export function generate_id(db: IDb): number {
    let max = 0;
    db.users.forEach(user => {
        if(user.id > max) {
            max = user.id;
        }
    })
    return max+1;
}


export function is_Id_Exsits(db: IDb, idToCheck: number): boolean {
    let found = false;
    db.users.forEach(user => {
        if(user.id === idToCheck) {
            found = true;
        }
    });
    return found;
}

