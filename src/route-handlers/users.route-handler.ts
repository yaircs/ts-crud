import express from 'express';
import db from '../db/db.userModel.js';
import { generate_id, is_Id_Exsits } from '../utils/utils.js';
import { IResponse, reqBody } from '../types/types.js'; 

const { DB_PATH } = process.env;

export async function getAllUsers() {
    const data = await db.read(DB_PATH as string);
    
    return {
        status: 200,
        message: 'success',
        data: data
    };
}

export async function getUserById(id: number) {
    const data = await db.read(DB_PATH as string);
    let user = {};

    if(is_Id_Exsits(data, id)) {
        user = data.users.filter(user => user.id === id);
        
        return {
            status: 200,
            message: "success",
            data: user
        } 
    } else throw new Error(`Id wasn't found`);
}

export async function createUser(reqBody: reqBody) {
    let data = await db.read(DB_PATH as string);
    
    const nextId = generate_id(data);
    const newUser = {
        id: nextId,
        name: reqBody.name,
        email: reqBody.email,
        phoneNumber: reqBody.phoneNumber
    }
    data.users.push(newUser);
    
    db.save(DB_PATH as string, data);

    return {
        status: 200,
        message: 'success',
        data: newUser
    }
}

export async function  patchUserById(id: number ,reqBody: reqBody): Promise<IResponse> {
    const validFields = ['name', 'email', "phoneNumber"];

    if(Object.keys(reqBody).some(field => validFields.includes(field))) {
        let data = await db.read(DB_PATH as string);

        if(is_Id_Exsits(data, id)) {
             const fieldsToUpdate = Object.keys(reqBody);
             let updatedUser;

             data.users.forEach(user => {
                 if(user.id === id) {
                     fieldsToUpdate.forEach(field => {
                         if(validFields.includes(field)) {
                             user[field as ('name'|'email'|'phoneNumber')] = reqBody[field];
                         }
                     });
                     updatedUser = user;
                 }
             })

             db.save(DB_PATH as string, data);

             return {
                status: 200,
                message: 'success',
                data: updatedUser
             };
        } else throw new Error(`Id wasn't found`);
    } else throw new Error('Update failed. No valid fields was found');


}

export async function putUserById(id: number ,reqBody: reqBody): Promise<IResponse> {
    const validFields = ['name', 'email', "phoneNumber"];
        
    if(validFields.every(field => Object.keys(reqBody).includes(field))) {
        let data = await db.read(DB_PATH as string);

        if(is_Id_Exsits(data, id)) {
            data.users.forEach((user, index) => {
                if(user.id === id) {
                    data.users[index].id = id;
                    data.users[index] = { ...data.users[index], ...reqBody };
                }
            });

            db.save(DB_PATH as string, data);

            return {
                status: 200,
                message: 'success',
                data: { id, ...reqBody }
            };
        } else throw new Error(`Id wasn't found`);
    } else throw new Error('Update failed. missing a field or more');
}

export async function deleteUserById(id: number): Promise<IResponse> {
    let data = await db.read(DB_PATH as string);

    if(is_Id_Exsits(data, id)) {
        let deletedUser = data.users.find(user => user.id === id);

        data.users = data.users.filter(user => {
            return user.id !== id;
        })

        db.save(DB_PATH as string, data);

        return {
            status: 200,
            message: 'success',
            data: deletedUser
        };

                
    } else throw new Error(`Id wasn't found`);
}