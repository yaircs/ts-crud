import express from'express';
export interface IUser {
    id: number,
    name: string,
    email: string,
    phoneNumber: string
}

export interface IDb {
    users: IUser[]
}

export interface IResponse {
    status: number;
    message: string;
    data: any;
}

export interface IError {
    status: number;
    message: string;
    stack?: string;
}

export interface reqBody {
    [key: string]: string
}