import express from 'express';
import fs from 'fs/promises';
import { IError } from '../types/types.js';

const { NODE_ENV } = process.env;

export const reqLogger = (path: string) => {
    return async (req: express.Request, res: express.Response, next:Function): Promise<void> => {
        const message = `[${new Date().toISOString()}] ${req.method} :: ${req.path}\n`
        await fs.appendFile(path, message);
        next();
    };
}

export const errLogger = (path: string) => {
    return async (error: Error, req: express.Request, res: express.Response, next: express.NextFunction): Promise<void>  => {
        const message = `[${new Date().toISOString()}] ${error.message} :: ${error.stack}\n`
        await fs.appendFile(path, message);
        next(error);
    }
}

export const respondWithError = (error: Error, req: express.Request, res: express.Response, next: express.NextFunction) => {
    let status;
    switch (error.message) {
        case `Id wasn't found`:
            status = 400;
            break;
        case 'Update failed. No valid fields was found':
            status = 400;
            break;
        case 'Update failed. missing a field or more':
            status = 400;
            break;
        default:
            status = 500;
    }

    const response: IError = { 
      status: status || 500, 
      message: error.message || 'Internal server error'
    };

    if(NODE_ENV !== 'production') response.stack = error.stack;
    res.status(response.status).json(response);
  }